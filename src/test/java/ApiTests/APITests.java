package test.java.ApiTests;

import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class APITests extends BaseAPITest{
    BaseAPITest baseAPITest = new BaseAPITest();

    @Test
    public void getOrder403() {

        given().
                auth().
                oauth2("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MDM3MDMyNjYsImV4cCI6MTYwNDMwODA2Niwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoia3VyZWxlbmtvdnF3ZXJ0eUBnbWFpbC5jb20ifQ.iw0TzzBybujTdzG-cmQx-R_uzaCgyH8HeS11QjRVViemAvw2KcVZPGf5ECccMOq2mkXofjQPCKINaSvBK9J5iqKwlAGMRwr5F9HTDgCDgmXrpI89C7X49RNAUYA-3-4KK7706a-dJqMKO_N2eMU1LaBSpncoyyroCo5jxl-8lKnKw4eHreUIUZXcSbWYGRL5ch6cLrrdPdSCnLcakYvaKF-i1e-Cguq91G483fBmPq8NSlPp2MNoiBdFv53g8COmCPhKdVEvX-42qCxY8q8PCBrn8ekXhSOqGADii09lWhIP0VMaWLfW9Q075Rz6nz-rry-qBInU48lxJYwsclcezHmCG0-_aSmj7OruEaXComRo3aPhpuvDcXsfEfauvkoSgNOm3whTiysvcUEZRHpE8Qz4b79qRJt9FzeUL6EEVvhwDTOE6qGDLCr_29GCyXjtLl9ME-dz6XFHnfnTzarYQcYxCCE67gGTOMmYNPklpcakrjEAZuo5CRGKxIuh2n69vUorAj2k8r-gObDL7k376GA8IG6rH1iSfSFp-c05BbhyWcrUx0MtjO0DyA17nhEw3ewZ2ZVWFYwB8Ihv4vkpoZI4yR-uhd4fzdEC8beSGzcXBbd2J3hfrYjSqfv5RPN_y2VBhH--Gf_BJ4KS3rnI4AEVCtDMogl1Onkcpnxwtv4").
                when().
                get("https://api.evakovrik.lazy-ants.com/api/orders/403").
                then().
                assertThat().
                spec(baseAPITest.checkStatusCodeAndContentType);
    }
}
